WIJS MISSION
============

##Structure

  * app/ => back-end
  * assets/ =>front-end
  * Gruntfile / package.json
  * Mock.psd
  * composer

##Syntax

  * Coffeescript / Sass(compass) SMACSS
  * Backbone / Underscore / Jquery
  * PHP 5.3 / Yaml / doc dbal
  * templating: underscore / twig

##Data

  * GET /offices
  * POST /offices/search

##Info

  * Next time => should use the third party ORM with silex instead of dbal
  * Questions? robbie.bardijn@gmail.com
  * Hope you like it.

