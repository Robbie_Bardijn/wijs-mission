<?php
  namespace Controller;
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\Response;

class Location {
  protected $app;

  public function __construct($app) {
    $this->app = $app;
  }

  private function clean($str) {
    if(trim($str) == "") {
      $this->app->abort(404);
    }

    return urlencode(trim($str));
  }

  private function is_ajax() {
    return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
  }

  public function all() {
    $offices = $this->app['db']->fetchAll("SELECT id, street, city, latitude, longitude, is_open_in_weekends, has_support_desk FROM `offices`");
    if ($this->is_ajax()) {
      return new Response(json_encode($offices), 200, array('Content-Type' => 'application/json'));
    } else {
      $this->app->abort(404);
    }
  }

  public function search (Request $request) {
    if ($this->is_ajax()) {

      // CHECKING POST VARS
      $loc = $this->clean($request->request->get("search"));
      $weekend = (!is_null($request->request->get("weekend")))? "Y" : "N";
      $support = (!is_null($request->request->get("support")))? "Y" : "N";

      $conn = $this->app['db'];

      // GETTING THE LAT LNG FOR THE SEARCHED CONTENT
      $url = str_replace(":LOC", $loc, $this->app['config']['config']['geo']);
      $g_resp = file_get_contents($url);
      $g_resp = json_decode($g_resp, true);
      $g_loc = $g_resp['results'][0]['geometry']['location'];

      // NOTE: This DBAL api is a minefield and badly documented!, also silex uses an older version => USE 3TH PARTY ORM NEXT TIME

      // GET IDS NEARBY WITH HAVERSINE FORMULA
      $qb = $conn->createQueryBuilder();
      $qb
        ->select('o.id', '( 6371 * acos( cos( radians(:lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:lng) ) + sin( radians(:lat) ) * sin( radians( latitude ) ) ) ) AS distance')
        ->from('offices', 'o')
        ->where($qb->expr()->andX(
            $qb->expr()->eq('o.is_open_in_weekends', ':is_open_in_weekends'),
            $qb->expr()->eq('o.has_support_desk', ':has_support_desk')
            )
          )
        ->having('distance < :dist')
        ->orderBy('distance');

      $stmt = $conn->prepare($qb);
      $stmt->bindValue("lat", $g_loc['lat']);
      $stmt->bindValue("lng", $g_loc['lng']);
      $stmt->bindValue("is_open_in_weekends", $weekend);
      $stmt->bindValue("has_support_desk", $support);
      $stmt->bindValue("dist", $this->app['config']['config']['dist']);
      $stmt->execute();

      $result = $stmt->fetchAll();
      $office_ids = $this->pluck($result, "id");

      // GET ALL VALUES from ids for clean model fillings in front-end
      $stmt = $conn->executeQuery(
        'SELECT id, street, city, latitude, longitude, is_open_in_weekends, has_support_desk FROM offices WHERE id IN (?)',
        array($office_ids),
        array(\Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
      );

      $offices = $stmt->fetchAll();

      // WTF! why is the result of the query extended with keys 0 to 6 REMOVE THEM for clean result in front end models
      foreach ($offices as &$value) {
        unset($value[0], $value[1], $value[2], $value[3], $value[4], $value[5], $value[6]);
      }

      return new Response(json_encode($offices), 200, array('Content-Type' => 'application/json'));
    } else {
      $this->app->abort(404);
    }
  }

  // QUICK USE OF AN ARRAY PLUCK METHOD SHOULD SIT IN A HELPERS CLASS OR SOMETHING EQUIVALENT
  public function pluck($array, $key, $index = null) {
    $return = array();
    $get_deep = strpos($key, '.') !== false;

    if ( ! $index) {
      foreach ($array as $i => $a) {
        $return[] = (is_object($a) and ! ($a instanceof \ArrayAccess)) ? $a->{$key} :
        ($get_deep ? static::get($a, $key) : $a[$key]);
      }
    } else {
      foreach ($array as $i => $a) {
        $index !== true and $i = (is_object($a) and ! ($a instanceof \ArrayAccess)) ? $a->{$index} : $a[$index];
        $return[$i] = (is_object($a) and ! ($a instanceof \ArrayAccess)) ? $a->{$key} :
        ($get_deep ? static::get($a, $key) : $a[$key]);
      }
    }
    return $return;
  }
}
?>
