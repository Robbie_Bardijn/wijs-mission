<?php
  //GENERALIZE LIBS
  $libs = array(
    "autoload" => __DIR__ . "/../vendor/autoload.php",
    "config" => __DIR__ . '/config/all.yml',
    "views" => __DIR__ . '/views'
  );

  //LOADING
  require_once $libs["autoload"];
  use Symfony\Component\HttpFoundation\Request;
  use Symfony\Component\HttpFoundation\Response;
  use DerAlex\Silex\YamlConfigServiceProvider;
  use Silex\Provider\DoctrineServiceProvider;
  use Silex\Provider\TwigServiceProvider;
  use Silex\Provider\ServiceControllerServiceProvider;
  use Controller\Location;

  //INIT
  $app = new Silex\Application();

  //REGISTERS
  $app->register(new YamlConfigServiceProvider($libs["config"]));
  $app->register(new DoctrineServiceProvider(), array('db.options' => $app['config']['database']));
  $app->register(new TwigServiceProvider(), array('twig.path' => $libs["views"]));
  $app->register(new ServiceControllerServiceProvider());

  if($app['config']['config']['mode'] == "dev"){
    $app['debug'] = true;
  }

  $app['controller.location'] = $app->share(function() use ($app) {
    return new Location($app);
  });

  // PAGES
  $app->get('', function (Request $request) use ($app) {
    return $app['twig']->render('layout.twig', array(
      'mode' => $app['config']['config']['mode']
    ));
  });

  // SERVICES
  $app->get('offices', "controller.location:all");
  $app->post('offices/search', "controller.location:search");

  // RUN APP
  $app->run();
