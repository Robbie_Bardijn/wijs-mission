(function() {
  var app, helper, site;

  helper = {
    track_event: function(category, label, value) {
      var err;
      try {
        if ((label != null) && (value != null)) {
          return _gaq.push(['_trackEvent', category, label, value]);
        } else if (label != null) {
          return _gaq.push(['_trackEvent', category, label]);
        } else {
          return _gaq.push(['_trackEvent', category]);
        }
      } catch (_error) {
        err = _error;
      }
    },
    track_page: function(frag) {
      var _gaq;
      _gaq = window._gaq || [];
      return _gaq.push(['_trackPageview', "/" + frag]);
    }
  };

  app = {
    model: {
      office: Backbone.Model.extend({
        defaults: {
          street: "Unknown Street",
          city: "Unknown City",
          latitude: 50.503887,
          longitude: 4.469936,
          is_open_in_weekends: "N",
          has_support_desk: "N"
        },
        give_open_in_weekends: function() {
          if (this.get("is_open_in_weekends") === "Y") {
            return "yes";
          } else {
            return "no";
          }
        },
        give_has_support_desk: function() {
          if (this.get("has_support_desk") === "Y") {
            return "yes";
          } else {
            return "no";
          }
        },
        give_lat_long: function() {
          return new google.maps.LatLng(this.get("latitude"), this.get("longitude"));
        },
        give_title: function() {
          return "" + (this.get("street")) + " ( " + (this.get("city")) + " )";
        },
        give_marker_template: function() {
          return _.template($("#aside_marker").html(), this.toJSON());
        },
        give_info_template: function() {
          return _.template($("#info_window").html(), this.toJSON());
        },
        remove_from_map: function() {
          return this.marker.setMap(null);
        },
        get_marker: function(map) {
          if (this.marker == null) {
            this.marker = new google.maps.Marker({
              position: this.give_lat_long(),
              map: map,
              title: this.give_title(),
              icon: {
                url: "/assets/img/sprite/marker.png",
                size: new google.maps.Size(17, 25),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(9, 25)
              }
            });
          } else {
            this.marker.setMap(map);
          }
          return this.marker;
        },
        give_info_window: function() {
          if (site.map.info == null) {
            site.map.info = new google.maps.InfoWindow({
              content: this.give_info_template()
            });
          } else {
            site.map.info.setContent(this.give_info_template());
          }
          return site.map.info;
        }
      })
    },
    collection: {
      offices: Backbone.Collection.extend({
        url: function() {
          return "/offices";
        }
      })
    }
  };

  site = {
    page: $('body').attr('data-page'),
    map: {
      center: function(latlng) {
        return this.map.setCenter(latlng);
      },
      center_group: function(collection) {
        var limits;
        limits = new google.maps.LatLngBounds();
        _.each(collection, (function(_this) {
          return function(model, index, list) {
            return limits.extend(model.get_marker(_this.map).position);
          };
        })(this));
        return this.map.fitBounds(limits);
      },
      initialize: function() {
        var mapOptions;
        mapOptions = {
          zoom: 8,
          center: new google.maps.LatLng(50.85034, 4.35171),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          styles: this.style,
          mapTypeControl: false,
          streetViewControl: false,
          overviewMapControl: false,
          panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
          },
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.TOP_RIGHT
          }
        };
        return this.map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
      }
    },
    config: {
      validation: {
        min: 2,
        max: 30
      }
    },
    index: function() {
      var add_all_markers, all_offices, check_val_search, submit_search;
      all_offices = {};
      add_all_markers = function() {
        all_offices = new app.collection.offices([], {
          model: app.model.office
        });
        return all_offices.fetch({
          success: function(collection) {
            return collection.each(function(model, index, list) {
              var marker;
              model.set({
                open_in_weekends: model.give_open_in_weekends(),
                support_desk: model.give_has_support_desk()
              });
              marker = model.get_marker(site.map.map);
              google.maps.event.addListener(marker, "click", function() {
                var info;
                info = model.give_info_window();
                info.open(site.map.map, marker);
                return site.map.center(model.give_lat_long());
              });
              if (list.length === index + 1) {
                return site.map.map.panBy(-160, 0);
              }
            });
          },
          error: function(collection, response) {}
        });
      };
      check_val_search = function(target) {
        var _ref;
        if ((site.config.validation.min <= (_ref = target.val().length) && _ref < site.config.validation.max)) {
          return $("#submit-search").removeClass("is-disabled");
        } else {
          return $("#submit-search").addClass("is-disabled");
        }
      };
      submit_search = function() {
        var $form;
        $form = $("#search-location");
        return $.ajax({
          url: $form.attr("action"),
          type: $form.attr("method"),
          data: $form.serializeArray()
        }).done(function(data, textStatus, jqXHR) {
          var bad_markers, found_offices, good_markers, ids;
          found_offices = new app.collection.offices(data, {
            model: app.model.office
          });
          $(".search-results > ul").empty();
          found_offices.each(function(model, index, list) {
            return $(".search-results > ul").append(model.give_marker_template());
          });
          ids = found_offices.pluck("id");
          bad_markers = all_offices.filter(function(element) {
            return !_.contains(ids, element.get("id"));
          });
          good_markers = all_offices.filter(function(element) {
            return _.contains(ids, element.get("id"));
          });
          _.each(bad_markers, function(value, index, list) {
            return value.remove_from_map();
          });
          return site.map.center_group(good_markers);
        });
      };
      $.getJSON("assets/json/mapstyle.json", function(json) {
        site.map.style = json;
        site.map.initialize();
        return add_all_markers();
      });
      $("#search-location").on("submit", function(e) {
        e.preventDefault();
        if (!$("#submit-search").hasClass("is-disabled")) {
          return submit_search();
        }
      });
      $("#submit-search").click(function(e) {
        e.preventDefault();
        if (!$("#submit-search").hasClass("is-disabled")) {
          return $("#search-location").submit();
        }
      });
      $("#open-close").click(function(e) {
        e.preventDefault();
        if ($(e.currentTarget).hasClass("close")) {
          $(e.currentTarget).attr("class", "search");
          $(e.currentTarget).parent().css("left", "0px");
          return $(".search-locaction").addClass("close");
        } else {
          $(e.currentTarget).parent().css("left", "320px");
          $(e.currentTarget).attr("class", "close");
          return $(".search-locaction").removeClass("close");
        }
      });
      $("#search").on("keyup", function(e) {
        return check_val_search($(e.currentTarget));
      });
      $(".search-results ul").on("click", "li a", function(e) {
        var id, mev, model;
        e.preventDefault();
        id = $(e.currentTarget).attr("data-id");
        model = all_offices.findWhere({
          "id": id
        });
        console.log(model);
        mev = {
          stop: null,
          latLng: model.give_lat_long()
        };
        return google.maps.event.trigger(model.marker, 'click', mev);
      });
      return check_val_search($("#search"));
    }
  };

  $(function() {
    if (typeof site[site.page] === "function") {
      return site[site.page]();
    }
  });

}).call(this);

//# sourceMappingURL=base.js.map
