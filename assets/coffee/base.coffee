# EXTRA HELPER CLASS FOR TRACKING !!! UNUSED FOR THIS PROJECT!!!
helper =
  track_event: (category, label, value) ->
      try
        # console.log "track: category: #{category}  - label: #{label}  - value:  #{value}"
        if label? and value?
          _gaq.push ['_trackEvent', category, label, value]
        else if label?
          _gaq.push ['_trackEvent', category, label]
        else
          _gaq.push ['_trackEvent', category]
      catch err
        # console.log "couldn't track event: category: #{category}  - label: #{label}  - value:  #{value}"

  track_page: (frag) ->
    # console.log "track: page: frag: #{frag}"
    _gaq = window._gaq || []
    _gaq.push(['_trackPageview', "/#{frag}"])

# APP OBJECT THAT HOLDS BB CLASSES
app =
  model:
    office : Backbone.Model.extend
      defaults:
        street: "Unknown Street"
        city: "Unknown City"
        latitude: 50.503887
        longitude: 4.469936
        is_open_in_weekends: "N"
        has_support_desk: "N"

      give_open_in_weekends: ->
        if @get("is_open_in_weekends") is "Y" then "yes" else "no"

      give_has_support_desk: ->
        if @get("has_support_desk") is "Y" then "yes" else "no"

      give_lat_long: ->
        new google.maps.LatLng @get("latitude"), @get("longitude")

      give_title: ->
        "#{@get("street")} ( #{@get("city")} )"

      give_marker_template: ->
        _.template $("#aside_marker").html(), @.toJSON()

      give_info_template: ->
        _.template $("#info_window").html(), @.toJSON()

      remove_from_map: ->
        @marker.setMap null

      get_marker: (map) ->
        # SINGLETON ONE INSTANCE OVER MODEL
        unless @marker?
          @marker = new google.maps.Marker
            position: @give_lat_long()
            map: map
            title: @give_title()
            icon:
              url: "/assets/img/sprite/marker.png"
              size: new google.maps.Size 17, 25
              origin: new google.maps.Point 0, 0
              anchor: new google.maps.Point 9, 25

        else
          @marker.setMap map
        return @marker

      give_info_window: ->
        # SINGLETON ONE INSTANCE OVER APP
        unless site.map.info?
          site.map.info = new google.maps.InfoWindow
            content: @give_info_template()
        else
          site.map.info.setContent @give_info_template()
        return site.map.info

  collection:
    offices : Backbone.Collection.extend
      url: -> "/offices"

site =
  page: $('body').attr 'data-page'
  map:
    center: (latlng) ->
      @map.setCenter latlng

    center_group: (collection) ->
      limits = new google.maps.LatLngBounds()

      _.each collection, (model, index, list) =>
        limits.extend model.get_marker(@map).position

      @map.fitBounds limits

    initialize: ->
      mapOptions =
        zoom: 8
        center: new google.maps.LatLng(50.85034, 4.35171)
        mapTypeId: google.maps.MapTypeId.ROADMAP
        styles: this.style
        mapTypeControl: false
        streetViewControl: false
        overviewMapControl: false
        panControlOptions:
          position: google.maps.ControlPosition.TOP_RIGHT
        zoomControlOptions:
          style: google.maps.ZoomControlStyle.LARGE
          position: google.maps.ControlPosition.TOP_RIGHT

      @map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions)

  config:
    validation:
      min: 2
      max: 30

  index: () ->
    all_offices = {};

    add_all_markers = ->
      all_offices = new app.collection.offices([], {model: app.model.office})
      all_offices.fetch
        success: (collection) ->
          collection.each (model, index, list) ->
            model.set open_in_weekends: model.give_open_in_weekends(), support_desk: model.give_has_support_desk()
            marker = model.get_marker site.map.map

            # CLICK EVENT
            google.maps.event.addListener marker, "click", ->
              info = model.give_info_window()
              info.open site.map.map, marker
              site.map.center model.give_lat_long()

            if list.length is index + 1
              site.map.map.panBy -160, 0

        error: (collection, response) ->
          # console.log "Server says #{response.status}."

    check_val_search = (target)->
      if site.config.validation.min <= target.val().length < site.config.validation.max
        $("#submit-search").removeClass "is-disabled"
      else
        $("#submit-search").addClass "is-disabled"

    submit_search = ->
      $form = $ "#search-location"
      $.ajax
        url: $form.attr "action"
        type: $form.attr "method"
        data: $form.serializeArray()
      .done (data, textStatus, jqXHR) ->
        found_offices = new app.collection.offices data, {model: app.model.office}
        $(".search-results > ul").empty();

        found_offices.each (model, index, list) ->
          $(".search-results > ul").append model.give_marker_template()

        ids = found_offices.pluck "id"

        bad_markers = all_offices.filter (element) ->
          not _.contains ids, element.get "id"

        good_markers = all_offices.filter (element) ->
          _.contains ids, element.get "id"

        _.each bad_markers, (value, index, list) ->
          value.remove_from_map()

        site.map.center_group good_markers

    # BUILD MAP, STYLE AND ADD MARKERS
    $.getJSON "assets/json/mapstyle.json", (json) ->
      site.map.style = json
      do site.map.initialize
      do add_all_markers

    $("#search-location").on "submit", (e) ->
      do e.preventDefault
      unless $("#submit-search").hasClass "is-disabled"
        do submit_search

    $("#submit-search").click (e) ->
      do e.preventDefault
      unless $("#submit-search").hasClass "is-disabled"
        do $("#search-location").submit

    $("#open-close").click (e) ->
      do e.preventDefault
      if $(e.currentTarget).hasClass "close"
        $(e.currentTarget).attr "class", "search"
        $(e.currentTarget).parent().css "left", "0px"
        $(".search-locaction").addClass "close"
      else
        $(e.currentTarget).parent().css "left", "320px"
        $(e.currentTarget).attr "class", "close"
        $(".search-locaction").removeClass "close"

    $("#search").on "keyup", (e) ->
      check_val_search $(e.currentTarget)

    # DELEGATE METHOD ON AJAXED RESULTS
    $(".search-results ul").on "click", "li a", (e) ->
      do e.preventDefault
      id = $(e.currentTarget).attr "data-id"
      model = all_offices.findWhere "id": id
      console.log model
      mev =
        stop: null
        latLng: model.give_lat_long()

      google.maps.event.trigger model.marker, 'click', mev

    # CHECK SEARCH BUTTON
    check_val_search $("#search")

# GENERALIZE DOCUMENT READY BASED ON data-page in body tag
$ ->
  if typeof site[site.page] is "function"
    do site[site.page]
